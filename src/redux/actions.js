export const GET_USERS = 'GET_USERS';
export const USERS_RECEIVED = 'USERS_RECEIVED';

export function getUsers() {
    return {
        type: GET_USERS
    }
}