import {GET_USERS, USERS_RECEIVED} from './actions';

function reducer(state = {}, action) {
    switch (action.type) {
        case GET_USERS:
            return {...state, loading: true};
        case USERS_RECEIVED:
            return {...state, users: action.json, loading: false};
        default:
            return state
    }
}

export default reducer