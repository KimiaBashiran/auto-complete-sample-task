import {createStore, applyMiddleware} from 'redux'
import createSagaMiddleware from 'redux-saga'
import thunkMiddleware from 'redux-thunk'

import rootReducer from './reducers'

const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
    return {
        ...createStore(
            rootReducer,
            applyMiddleware(sagaMiddleware, thunkMiddleware),
        ),
        runSaga: sagaMiddleware.run
    }
}