import React from 'react';
import {connect} from 'react-redux'
import './auto-complete-component.scss';
import {equals} from "../../_helper/helpers";

class AutoCompleteComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fieldSetLegendWidth: 0,
            inputFocus: false,
            autoCompleteValue: this.props.initialValue || '',
            dropDownItems: this.props.users || [],
        };

        this.originalValues = [];

        this.container = React.createRef();
        this.onAutoCompleteFocus = this.onAutoCompleteFocus.bind(this);
        this.onAutoCompleteBlur = this.onAutoCompleteBlur.bind(this);
        this.onAutoCompleteChange = this.onAutoCompleteChange.bind(this);
    }

    componentDidMount() {
        if (this.props.placeHolder) {
            this.widthOfPlaceHolderLegend('auto-complete-label');
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!equals(prevProps.users, this.props.users)) {
            this.setState({dropDownItems: this.props.users});
            this.originalValues = this.props.users;
        }
    }

    widthOfPlaceHolderLegend(placeHolderClassName) {
        if (!this.state.fieldSetLegendWidth) {
            let elementClientRects = this.container.current.getElementsByClassName(placeHolderClassName)[0].getClientRects()[0];
            if (elementClientRects) {
                let legendWidth = Math.round(elementClientRects.width * 0.9 + 8);
                this.setState({fieldSetLegendWidth: legendWidth});
                return legendWidth;
            }
        }
        else {
            return this.state.fieldSetLegendWidth;
        }
    }

    onAutoCompleteFocus() {
        this.setState({inputFocus: true});
    }

    onAutoCompleteBlur() {
        setTimeout(() => { //allowing component to set the value
            this.setState({inputFocus: false})
        }, 100);
    }

    onAutoCompleteChange(event) {
        event.persist();
        this.setState({autoCompleteValue: event.target.value}, () => {
            if (event.target.value) {
                this.filterSearchResult(event.target.value);
            }
            else {
                this.emptySearchResult();
            }
        })
    }

    filterSearchResult(value) {
        let filteredValues = this.originalValues.filter(singleValue => singleValue.name.toLowerCase().includes(value.toLowerCase()));
        this.setState({dropDownItems: filteredValues});
    }

    emptySearchResult() {
        this.setState({dropDownItems: this.originalValues});
    }

    onDropDownItemClick(singleItem) {
        this.setState({autoCompleteValue: singleItem.name});
        this.props.onListSelect(singleItem);
    }

    render() {
        return (
            <div className="auto-complete-holder" ref={this.container}>
                <fieldset className="auto-complete">
                    <legend className="auto-complete-legend"
                            style={{width: (this.state.autoCompleteValue || this.state.inputFocus) ? this.state.fieldSetLegendWidth + 'px' : 0}}/>
                    <label className={"auto-complete-label" + ((this.state.autoCompleteValue || this.state.inputFocus) ? " active" : '')}>
                        {this.props.placeHolder}
                    </label>
                    <input type="text" className="auto-complete-input" value={this.state.autoCompleteValue}
                           onFocus={this.onAutoCompleteFocus} onBlur={this.onAutoCompleteBlur}
                           onChange={this.onAutoCompleteChange}/>
                    {this.props.loading ?
                        <i className="loading-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
                                 viewBox="20 20 60 60" preserveAspectRatio="xMidYMid">
                                <g transform="rotate(0 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.9166666666666666s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(30 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.8333333333333334s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(60 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.75s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(90 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.6666666666666666s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(120 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.5833333333333334s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(150 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.5s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(180 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.4166666666666667s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(210 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.3333333333333333s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(240 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.25s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(270 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.16666666666666666s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(300 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                                 begin="-0.08333333333333333s" repeatCount="indefinite"/>
                                    </rect>
                                </g>
                                <g transform="rotate(330 50 50)">
                                    <rect x="47.5" y="24" rx="2.5" ry="2.88" width="5" height="12" fill="#656565">
                                        <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s"
                                                 repeatCount="indefinite"/>
                                    </rect>
                                </g>
                            </svg>
                        </i>
                        :
                        null
                    }
                </fieldset>
                <div className="drop-down-holder" style={{maxHeight: this.state.inputFocus ? 200 + 'px' : 0}}>
                    <ul className="drop-down">
                        {!this.state.dropDownItems.length ?
                            <li className="single-drop-down-item">
                                No results found!
                            </li>
                            :
                            [this.state.dropDownItems.map((singleItem, index) =>
                                <li className="single-drop-down-item" key={index}
                                    onClick={() => this.onDropDownItemClick(singleItem)}>
                                    {singleItem.name}
                                </li>
                            )]
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users,
        loading: state.loading,
    }
};

export default connect(mapStateToProps, null)(AutoCompleteComponent);
