import React from 'react';
import {connect} from 'react-redux';
import './App.scss';
import AutoCompleteComponent from "./components/auto-complete-component/auto-complete-component";
import {getUsers} from "./redux/actions";

class App extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        return (
            <div className="App">
                <h1 className="page-title">Sample Task</h1>
                <p className="page-description">an autocomplete with suggestions from a mock API</p>

                <AutoCompleteComponent placeHolder={"User Name"} onListSelect={(data) => {
                    console.log(data)
                }}/>
            </div>
        );
    }
}

const mapDispatchToProps = {
    getUsers: getUsers,
};

export default connect(null, mapDispatchToProps)(App);
